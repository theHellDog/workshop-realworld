Requirements:
```
Docker
Docker Compose
```

Preparations:
```
docker-compose pull
docker-compose build
docker-compose run frontend npm install
docker-compose run backend npm install
```

Run the site locally:
```
docker-compose up
```
